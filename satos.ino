#include <WiFi.h>
#include <WebServer.h>


const char* ssid = "GuiEtJew";
const char* password = "lithium est notre chat.";

WebServer server(80);

void setup() {
  Serial.begin(115200);
  Serial.setDebugOutput(true);
  Serial.println();



  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  Serial.print("Camera Ready! Use 'http://");
  Serial.print(WiFi.localIP());
  Serial.println("' to connect");


  
  server.on("/", handle_OnConnect);
  server.on("/led1on", handle_led1on);
  server.onNotFound(handle_NotFound);
  
  server.begin();
  
}

void loop() {
  server.handleClient();
}
